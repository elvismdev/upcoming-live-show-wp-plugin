<?php
/**
 * List Live Shows.
 *
 * @package WordPress
 * @subpackage Next Live Show Plugin
 */

defined('ABSPATH') or die('No script kiddies please!');

require_once(ABSPATH . 'wp-admin/includes/template.php');
if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class My_List_Table extends WP_List_Table
{
    private $rows;
    private $count;
    private $per_page = 10;

    function __construct()
    {
        global $status, $page, $wpdb;

        parent::__construct(array(
            'singular' => __('book', 'mylisttable'), //singular name of the listed records
            'plural' => __('books', 'mylisttable'), //plural name of the listed records
            'ajax' => false //does this table support ajax?
        ));

        $orderby = isset($_GET['orderby']) ? $_GET['orderby'] : 'show_day, show_time';
        $order = isset($_GET['order']) ? $_GET['order'] : 'asc';

        $this->count = $wpdb->get_var( 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'live_shows' );

        $start = 0;
        $limit = $this->per_page;
        if ($this->get_pagenum() > 1) {
            $limit = $this->get_pagenum() * $this->per_page;
            $start = $limit - $this->per_page;
        }

        $this->rows = $wpdb->get_results('SELECT * from ' . $wpdb->prefix . 'live_shows ORDER BY ' . $orderby . ' ' . $order . ' LIMIT ' . $start . ', ' . $limit, ARRAY_A);
    }

    function get_columns()
    {
        $columns = array(
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'URL',
            'show_day' => 'Week Day',
            'show_time' => 'Time'
        );
        return $columns;
    }

    function get_sortable_columns()
    {
        $sortable_columns = array(
            'name' => array('name', false),
            'url' => array('url', false),
            'show_day' => array('show_day', false),
            'show_time' => array('show_time', false)
        );
        return $sortable_columns;
    }

    function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = array('id');
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->set_pagination_args( array(
            'total_items' => $this->count,                  // WE have to calculate the total number of items
            'per_page'    => $this->per_page                     // WE have to determine how many items to show on a page
        ) );
        $this->items = $this->rows;
    }

    function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'id':
            case 'name':
            case 'url':
            case 'show_day':
            case 'show_time':
                return $item[$column_name];
                break;
            default:
                return print_r($item, true); // Show the whole array for troubleshooting DEBUG purposes
                break;
        }
    }

    function column_name($item)
    {
        $actions = array(
            'edit' => sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>', $_REQUEST['page'], 'edit', $item['id']),
            'delete' => sprintf('<a href="?page=%s&action=%s&id=%s" onclick="return confirm(\'Drop?\')">Delete</a>', 'live-shows', 'delete', $item['id']),
        );

        return '<strong>'. sprintf('%1$s %2$s', $item['name'], $this->row_actions($actions)) . '</strong>';
    }
}

function live_show_list()
{
    global $action;

    switch ($action) {
        case 'new':
            live_show_create();
            break;
        case 'edit':
            live_show_update();
            break;
        case 'delete':
            live_show_update();
            break;
        default:
            $myListTable = new My_List_Table();
            echo '<div class="wrap"><h2>Live Shows <a href="?page=live-shows&action=new" class="add-new-h2">Add New</a></h2>';
            echo '<p>Place the shortcode <code>[uls_output]</code> on any post or page to output the next LIVE show.</p>';
            $myListTable->prepare_items();
            $myListTable->display();
            echo '</div>';
            break;
    }
}


