<?php
/**
 * Delete/Update Live Shows.
 *
 * @package WordPress
 * @subpackage Next Live Show Plugin
 */

defined('ABSPATH') or die('No script kiddies please!');

function live_show_update()
{
    global $wpdb, $action, $days_week;

    $id = isset($_GET['id']) ? $_GET['id'] : 0;

    $show = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'live_shows WHERE id=%d', $id));
    $name = $show->name;
    $show_day = $show->show_day;
    $show_time = $show->show_time;
    $url = $show->url;

    ?>
    <div class="wrap">
        <h2>Update Live Show</h2>
        <form method="post" action="<?php echo admin_url('admin.php?page=live-shows'); ?>">
            <table class='wp-list-table widefat fixed'>
                <tr>
                    <th>Name</th>
                    <td><input type="text" name="name" value="<?php echo $name; ?>" required=""/></td>
                </tr>
                <tr>
                    <th>URL</th>
                    <td><input type="text" name="url" value="<?php echo $url; ?>" required=""/></td>
                </tr>
                <tr>
                    <th>Week Day</th>
                    <td><select name="show_day">
                        <?php
                        foreach ($days_week as $d) {
                            if ($d == $show_day)
                                echo '<option value="'.$d.'" selected>'.$d.'</option>';
                            else
                                echo '<option value="'.$d.'">'.$d.'</option>';
                        }
                        ?>
                    </select></td>
                </tr>
                <tr>
                    <th>Time</th>
                    <td><input type="text" name="show_time" id="show_time" value="<?php echo $show_time; ?>" required=""/></td>
                </tr>
            </table>
            <br>
            <input type='hidden' name="id" value='<?php echo $id; ?>'>
            <input type='submit' name="update" value='Save' class='button'>
            <input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Drop?')">
            <input type='button' name="cancel" value='Cancel' class='button' onclick="window.location.href = '<?php echo admin_url('admin.php?page=live-shows') ?>';">
        </form>
    </div>

    <script type="text/javascript">
        jQuery(function () {
            jQuery('#show_time').timepicker({
                showMinute: false
            });
        });
    </script>
    <?php
}