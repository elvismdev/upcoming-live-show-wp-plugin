<?php
/**
 * Plugin Name: Upcoming Live Show
 * Plugin URI: https://bitbucket.org/grantcardone/upcoming-live-show-wp-plugin
 * Description: Shortcode to dynamically display the upcoming live shows on witnation.com, ex: [uls_output]
 * Version: 1.1.0
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.2.1
 * License: GPL2
 */


defined('ABSPATH') or die('No script kiddies please!');

date_default_timezone_set('EST');

global $wpdb;

// load the scripts on only the plugin admin page 
if (isset($_GET['page']) && ($_GET['page'] == 'live-shows')) {
    // if we are on the plugin page, enable the script 
    add_action( 'admin_enqueue_scripts', 'enqueue_and_register_uls_scripts' );
}

function enqueue_and_register_uls_scripts() {

    // Register the script like this for a plugin:
    wp_enqueue_script('jquery-ui', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js');

    wp_register_script('tek_tour_slider', plugins_url('/js/jquery-ui-sliderAccess.js', __FILE__));
    wp_enqueue_script('tek_tour_slider', plugins_url('/js/jquery-ui-sliderAccess.js', dirname(__FILE__)));

    wp_register_script('tek_tour_timepicker', plugins_url('/js/jquery-ui-timepicker-addon.js', __FILE__));
    wp_enqueue_script('tek_tour_timepicker', plugins_url('/js/jquery-ui-timepicker-addon.js', dirname(__FILE__)));

    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

}

$action = isset($_GET['action']) ? $_GET['action'] : '';
$notice = '';

$id = isset($_GET['id']) ? $_GET['id'] : 0;
if (!$id)
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
$name = isset($_POST['name']) ? $_POST['name'] : '';
$show_day = isset($_POST['show_day']) ? $_POST['show_day'] : '';
$show_time = isset($_POST['show_time']) ? $_POST['show_time'] : '';
$url = isset($_POST['url']) ? $_POST['url'] : '';

if (isset($_POST['insert'])) {
    $result = $wpdb->insert(
        $wpdb->prefix . 'live_shows',
        array('name' => $name, 'show_day' => $show_day, 'show_time' => $show_time, 'url' => $url),
        array('%s', '%s', '%s')
        );

    if (!$result) {
        $notice = 'Error adding Live Show';
        add_action('admin_notices', 'notice_error');
    } else {
        $notice = 'Live Show created';
        add_action('admin_notices', 'notice_success');
    }
} elseif (isset($_POST['update'])) {
    $result = $wpdb->update(
        $wpdb->prefix . 'live_shows',
        array('name' => $name, 'show_day' => $show_day, 'show_time' => $show_time, 'url' => $url),
        array('id' => $id),
        array('%s', '%s', '%s', '%s'),
        array('%d')
        );

    if ($result === false) {
        $notice = 'Error updating Live Show';
        add_action('admin_notices', 'notice_error');
    } elseif ($result === 0) {
        $notice = 'Nothing Changed';
        add_action('admin_notices', 'notice_noupdate');
    } else {
        $notice = 'Live Show Updated';
        add_action('admin_notices', 'notice_success');
    }
} elseif (isset($_POST['delete']) || $action == 'delete') {
    $result = $wpdb->query($wpdb->prepare('DELETE FROM ' . $wpdb->prefix . 'live_shows WHERE id = %d', $id));

    if (!$result) {
        $notice = 'Error deleting Live Show';
        add_action('admin_notices', 'notice_error');
    } else {
        $notice = 'Live Show deleted';
        add_action('admin_notices', 'notice_success');
    }
}

function notice_success() {
    global $notice;

    echo '<div class="updated"><p>';
    _e( $notice, 'my-text-domain' );
    echo '</p></div>';
}

function notice_noupdate() {
    global $notice;

    echo '<div class="update-nag"><p>';
    _e( $notice, 'my-text-domain' );
    echo '</p></div>';
}

function notice_error() {
    global $notice;

    echo '<div class="error"><p>';
    _e( $notice, 'my-text-domain' );
    echo '</p></div>';
}

// [uls_output] shortcode
add_shortcode('uls_output', 'uls_shortcode');

$days_week = array(
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',
    'Sunday' => 'Sunday'
    );

function uls_shortcode()
{
    global $wpdb;

    $show = null;
    for ($i = 0; $i <= 6; $i++) {
        if ($i) {
            $xday = new \DateTime("today + $i day");
            $show = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'live_shows WHERE show_day = %s ORDER BY show_time', $xday->format('l')));
        } else {
            $today = new \DateTime('today');
            $show = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'live_shows WHERE show_day = %s AND show_time > %s ORDER BY show_time', $today->format('l'), $today->format('H:m')));
        }
        if ($show)
            break;
    }

    $html = '';
    if ($show) {
        $xday = new \DateTime();
        $xday->setTimestamp(strtotime($show->show_day));
        $html = '<div class="ghostdiv">
        <div class="ghost-container">
            <div>
                <h3>Upcoming Live Event</h3>
            </div>
            <div>
                <h2><a style="color: #ffffff;" href="' . $show->url . '">' . $show->name . '</a></h2>
            </div>
            <div class="hidden-md hidden-sm visible-xs">
                <h4>' . $show->show_day . ', ' . $xday->format('F jS\,  Y') . ' at ' . date("g:ia", strtotime($show->show_time)) . ' EST</h4>
            </div>
        </div>
    </div>
    ';
}

return $html;
}

function upcoming_live_show_options_install()
{
    global $wpdb;

    $db_name = $wpdb->prefix . 'live_shows';

    // Create database table
    if ($wpdb->get_var("show tables like '$db_name'") != $db_name) {
        $sql = "CREATE TABLE " . $db_name . " (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(25) CHARACTER SET utf8 NOT NULL, `show_day` varchar(10) NOT NULL, `show_time` time NOT NULL, `url` varchar(255) CHARACTER SET utf8 NOT NULL,PRIMARY KEY (`id`));";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

// Run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'upcoming_live_show_options_install');

// Dashboard MENU
add_action('admin_menu', 'upcoming_live_show_dashboard_menu');

function upcoming_live_show_dashboard_menu()
{
    add_menu_page('Shows',
        'Upcoming Live Show',
        'manage_options',
        'live-shows',
        'live_show_list',
        plugin_dir_url( __FILE__ ) . 'img/show-icon.png'
        );

    add_submenu_page('Shows',
        'Add New Live Show',
        'Add New',
        'manage_options',
        'live-show-create',
        'live_show_create');

    add_submenu_page(null,
        'Update Live Show',
        'Update',
        'manage_options',
        'live-show-update',
        'live_show_update');
}

define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'live-shows.php');
require_once(ROOTDIR . 'live-show-create.php');
require_once(ROOTDIR . 'live-show-update.php');


