<?php
/**
 * Create Live Shows.
 *
 * @package WordPress
 * @subpackage Next Live Show Plugin
 */

defined('ABSPATH') or die('No script kiddies please!');

function live_show_create()
{
    global $days_week;

    ?>
    <div class="wrap">
        <h2>Add New Live Show</h2>
        <form method="post" action="<?php echo admin_url('admin.php?page=live-shows'); ?>">
            <!-- <p>Output next LIVE show placing the shortcode <code>[uls_output]</code> on any post or page.</p> -->
            <table class='wp-list-table widefat fixed'>
                <tr>
                    <th>Name</th>
                    <td><input type="text" name="name" value="" required=""/></td>
                </tr>
                <tr>
                    <th>URL</th>
                    <td><input type="text" name="url" value="" required=""/></td>
                </tr>
                <tr>
                    <th>Week Day</th>
                    <td><select name="show_day">
                        <?php
                        foreach ($days_week as $d) {
                            echo '<option value="'.$d.'">'.$d.'</option>';
                        }
                        ?>
                    </select>
                </tr>
                <tr>
                    <th>Time</th>
                    <td><input type="text" name="show_time" id="show_time" value="" required=""/></td>
                </tr>
            </table>
            <br>
            <input type='submit' name="insert" value='Save' class='button'>
            <input type='button' name="cancel" value='Cancel' class='button' onclick="window.location.href = '<?php echo admin_url('admin.php?page=live-shows') ?>';">
        </form>
    </div>

    <script type="text/javascript">
        jQuery(function () {
            jQuery('#show_time').timepicker({
                showMinute: false
            });
        });
    </script>
    <?php
}